import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux';
import generateStore from './redux/store';
// import { StyledEngineProvider } from '@mui/material/styles';
// import { WrappedApp } from "./App";
import App from './App.jsx'
// import './index.css'

const store = generateStore();
// const WithStore = () => <Provider store={store}><WrappedApp /></Provider>
const WithStore = () => <Provider store={store}><App/></Provider>


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    {/* <StyledEngineProvider injectFirst> */}
      {/* <App /> */}
      <WithStore />
      {/* <WrappedApp /> */}
    {/* </StyledEngineProvider> */}
  </React.StrictMode>,
)
