import { all } from 'redux-saga/effects';
import GetCampuAction from '../components/campus/campus.saga';
import InsertCampuAction from '../components/campus/campus.saga';


export default function* rootSaga() {
    yield all([
        GetCampuAction(),
        InsertCampuAction()
    ]);
};

