// import { useState } from 'react'
import { HashRouter, Routes, Route } from "react-router-dom";
import Home from './components/Home'
// import './App.css'

function App() {


  return (
    <HashRouter>
        <Routes>
          {/* <Route path="new" element={<New />} />
          <Route path="auto" element={<Auto />} />
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} /> */}
          <Route path="/" element={<Home />} />
        </Routes>
    </HashRouter>
   
    
  )
}

export default App

// export function WrappedApp() {
//   return (
//     <HashRouter>
//       <App />
//     </HashRouter>
//   );
// }
