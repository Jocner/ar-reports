
import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";
import {
  GET_CAMPUS,
  GET_CAMPUS_START,
  GET_CAMPUS_SUCCESS,
  GET_CAMPUS_ERROR,
  GET_CAMPUS_FINALLY,
  INSERT_CAMPUS,
  INSERT_CAMPUS_START,
  INSERT_CAMPUS_SUCCESS,
  INSERT_CAMPUS_ERROR,
  INSERT_CAMPUS_FINALLY
} from "../../redux/actions";

function* GetCampuAction({ payload: {  } }) {
  yield put(GET_CAMPUS_START());


  try {


    const headers = {
      headers: {
        // Authorization: `Bearer ${token}`,
        'accept': 'application/json'
      },
    };

  
    const { data } = yield call(
      axios.get,
      import.meta.env.VITE_API_CAMPUS,
      headers
    );
    console.log('desde saga get', data)
    return yield put(GET_CAMPUS_SUCCESS(data));
  } catch (error) {
    console.log(error.message);
    yield put(GET_CAMPUS_ERROR(error.message));
  } finally {
    yield put(GET_CAMPUS_FINALLY());
  }
}

function* InsertCampuAction({ payload: { campu } }) {
  yield put(INSERT_CAMPUS_START());
  // let token = keycloak.token;

  try {
    // const isTokenUpdated = yield call(keycloak.updateToken, 5);
    // token = isTokenUpdated ? keycloak.token : token;

    // const headers = {
    //   headers: {
    //     Authorization: `Bearer ${token}`,
    //     // 'accept': 'application/json'
    //   },
    // };


   
    const { data } = yield call(
      axios.post,
      import.meta.env.VITE_API_CAMPUS,
      campu
    );
//  console.log("desde saga campus", data)
    return yield put(INSERT_CAMPUS_SUCCESS(data));
  } catch (error) {

    console.log(INSERT_CAMPUS_ERROR(error.message));
    yield put(INSERT_CAMPUS_ERROR(error.message));
  } finally {
    yield put(INSERT_CAMPUS_FINALLY());
  }
}

export default function* actionsWatcher() {
  yield takeLatest(GET_CAMPUS, GetCampuAction);
  yield takeLatest(INSERT_CAMPUS, InsertCampuAction);
}
