import { handleActions } from 'redux-actions';

import {
   
    GET_CAMPUS_START,
    GET_CAMPUS_SUCCESS,
    GET_CAMPUS_ERROR,
    GET_CAMPUS_FINALLY,
    INSERT_CAMPUS_START,
    INSERT_CAMPUS_SUCCESS,
    INSERT_CAMPUS_ERROR,
    INSERT_CAMPUS_FINALLY
 

} from '../../redux/actions';

const initialState = {
    error: null,
    isLoadingcampu: true,
    campu: [],
    messages: 'messages',
    max: null
};

export default handleActions({
    [GET_CAMPUS_START]: state => ({
        ...state,
        error: false,
        isLoadingcampu: true,
    }),
        [GET_CAMPUS_SUCCESS]: (state, action) => ({    
        ...state,
        campu: action.payload
    }),
    [GET_CAMPUS_ERROR]: (state, action) => ({
        ...state,
        error: action.payload,
    }),
    [GET_CAMPUS_FINALLY]: state => ({
        ...state,
        isLoadingcampu: false
    }),


    [INSERT_CAMPUS_START]: state => ({
        ...state,
        error: false,
        isLoading: true,
    }),
    [INSERT_CAMPUS_SUCCESS]: (state, action) => ({
        ...state,
        campu: [...state.campu, action.payload],
    }),
    [INSERT_CAMPUS_ERROR]: (state, action) => ({
        ...state,
        error: action.payload,
    }),
    [INSERT_CAMPUS_FINALLY]: state => ({
        ...state,
        isLoading: false
    }),

}, initialState);


