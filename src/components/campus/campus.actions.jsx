import { createAction } from 'redux-actions';

export const GET_CAMPUS = createAction('GET_CAMPUS');
export const GET_CAMPUS_START = createAction('GET_CAMPUS_START');
export const GET_CAMPUS_SUCCESS = createAction('GET_CAMPUS_SUCCESS');
export const GET_CAMPUS_ERROR = createAction('GET_CAMPUS_ERROR');
export const GET_CAMPUS_FINALLY = createAction('GET_CAMPUS_FINALLY');

export const INSERT_CAMPUS = createAction('INSERT_CAMPUS');
export const INSERT_CAMPUS_START = createAction('INSERT_CAMPUS_START');
export const INSERT_CAMPUS_SUCCESS = createAction('INSERT_CAMPUS_SUCCESS');
export const INSERT_CAMPUS_ERROR = createAction('INSERT_CAMPUS_ERROR');
export const INSERT_CAMPUS_FINALLY = createAction('INSERT_CAMPUS_FINALLY');

