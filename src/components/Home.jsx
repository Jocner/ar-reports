import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import CameraIcon from '@mui/icons-material/PhotoCamera';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import puentealto from '../assets/puentealto.jpeg'
import santiagocentro from '../assets/santiago.jpeg'
import montevideo from '../assets/montevideo.jpeg'
import { GET_CAMPUS } from '../redux/actions';

function Copyright() {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

// const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const cards = [{id: 1, campus: 'Puente Alto', img: `${puentealto}?w=248&fit=crop&auto=format`}, 
{id: 2, campus: 'Santiago Centro', img: `${santiagocentro}?w=248&fit=crop&auto=format`}, 
{id: 3, campus: 'Montevideo', img: `${montevideo}?w=248&fit=crop&auto=format`}, 
{id: 4, campus: 'Santiago Centro', img: '../assets/santiago.jpeg'}, 
{id: 5, campus: 'Montevideo', img: '../assets/montevideo.jpeg'},
{id: 6, campus: 'Santiago Centro', img: '../assets/santiago.jpeg'}];


// TODO remove, this demo shouldn't need to reset the theme.
const defaultTheme = createTheme();





function Home() {
  const dispatch = useDispatch();
  const { campu } = useSelector((state) => state.campu);

  useEffect(() => {

    dispatch(GET_CAMPUS({ }));

    // eslint-disable-next-line react-hooks/exhaustive-deps

  }, []);
  
  // console.log('desde el componente', campu)

  return (
    
    <ThemeProvider theme={defaultTheme}>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          {/* <CameraIcon sx={{ mr: 2 }} />
          <Typography variant="h6" color="inherit" noWrap>
            Album layout
          </Typography> */}
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: 'background.paper',
            pt: 8,
            pb: 6,
          }}
        >
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              Reports the Campus
            </Typography>
            <Typography variant="h5" align="center" color="text.secondary" paragraph>
              Something short and leading about the collection below—its contents,
              the creator, etc. Make it short and sweet, but not too short so folks
              don&apos;t simply skip over it entirely.
            </Typography>
            <Stack
              sx={{ pt: 4 }}
              direction="row"
              spacing={2}
              justifyContent="center"
            >
              <Button variant="contained">Main call to action</Button>
              <Button variant="outlined">Secondary action</Button>
            </Stack>
          </Container>
        </Box>
        <Container sx={{ py: 8 }} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {cards.map((card) => (
              <Grid item key={card.id} xs={12} sm={6} md={4}>
                <Card
                  sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                >
                  <CardMedia
                    component="div"
                    sx={{
                      // 16:9
                      pt: '56.25%',
                    }}
                    image={card.img}
                    // image={`${puentealto}?w=248&fit=crop&auto=format`}
                  />
                  {/* <img src={`${puentealto}?w=248&fit=crop&auto=format`}/> */}
                  {/* <img src={`${card.img}?w=248&fit=crop&auto=format`}/> */}
                  <CardContent sx={{ flexGrow: 1 }}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {card.campus}
                    </Typography>
                    <Typography>
                      This is a media card. You can use this section to describe the
                      content.
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small">View</Button>
                    <Button size="small">Edit</Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
      </main>
      {/* Footer */}
      <Box sx={{ bgcolor: 'background.paper', p: 6 }} component="footer">
        <Typography variant="h6" align="center" gutterBottom>
       
          Footer
        </Typography>
        <Typography
          variant="subtitle1"
          align="center"
          color="text.secondary"
          component="p"
        >
          Something here to give the footer a purpose!
        </Typography>
        <Copyright />
      </Box>
      {/* End footer */}
    </ThemeProvider>
   
  )
}

export default Home