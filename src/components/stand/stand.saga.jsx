
import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";
import {
    GET_STAND,
    GET_STAND_START,
    GET_STAND_SUCCESS,
    GET_STAND_ERROR,
    GET_STAND_FINALLY,

    INSERT_STAND,
    INSERT_STAND_START,
    INSERT_STAND_SUCCESS,
    INSERT_STAND_ERROR,
    INSERT_STAND_FINALLY
} from "../../redux/actions";

function* GetStandAction({ payload: {  } }) {
  yield put(GET_STAND_START());


  try {


    const headers = {
      headers: {
        // Authorization: `Bearer ${token}`,
        'accept': 'application/json'
      },
    };

  
    const { data } = yield call(
      axios.get,
      import.meta.env.VITE_API_CAMPUS,
      headers
    );
    console.log('desde saga get', data)
    return yield put(GET_STAND_SUCCESS(data));
  } catch (error) {
    console.log(error.message);
    yield put(GET_STAND_ERROR(error.message));
  } finally {
    yield put(GET_STAND_FINALLY());
  }
}

function* InsertStandAction({ payload: { stand } }) {
  yield put(INSERT_STAND_START());
  // let token = keycloak.token;

  try {
    // const isTokenUpdated = yield call(keycloak.updateToken, 5);
    // token = isTokenUpdated ? keycloak.token : token;

    // const headers = {
    //   headers: {
    //     Authorization: `Bearer ${token}`,
    //     // 'accept': 'application/json'
    //   },
    // };


   
    const { data } = yield call(
      axios.post,
      import.meta.env.VITE_API_CAMPUS,
      stand
    );
//  console.log("desde saga campus", data)
    return yield put(INSERT_STAND_SUCCESS(data));
  } catch (error) {

    console.log(INSERT_STAND_ERROR(error.message));
    yield put(INSERT_STAND_ERROR(error.message));
  } finally {
    yield put(INSERT_STAND_FINALLY());
  }
}

export default function* actionsWatcher() {
  yield takeLatest(GET_STAND, GetStandAction);
  yield takeLatest(INSERT_STAND, InsertStandAction);
}
