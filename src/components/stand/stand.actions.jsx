import { createAction } from 'redux-actions';

export const GET_STAND = createAction('GET_STAND');
export const GET_STAND_START = createAction('GET_STAND_START');
export const GET_STAND_SUCCESS = createAction('GET_STAND_SUCCESS');
export const GET_STAND_ERROR = createAction('GET_STAND_ERROR');
export const GET_STAND_FINALLY = createAction('GET_STAND_FINALLY');

export const INSERT_STAND = createAction('INSERT_STAND');
export const INSERT_STAND_START = createAction('INSERT_STAND_START');
export const INSERT_STAND_SUCCESS = createAction('INSERT_STAND_SUCCESS');
export const INSERT_STAND_ERROR = createAction('INSERT_STAND_ERROR');
export const INSERT_STAND_FINALLY = createAction('INSERT_STAND_FINALLY');

