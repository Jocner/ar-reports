import { handleActions } from 'redux-actions';

import {
   
    GET_STAND_START,
    GET_STAND_SUCCESS,
    GET_STAND_ERROR,
    GET_STAND_FINALLY,

    INSERT_STAND_START,
    INSERT_STAND_SUCCESS,
    INSERT_STAND_ERROR,
    INSERT_STAND_FINALLY
 

} from '../../redux/actions';

const initialState = {
    error: null,
    isLoadingstand: true,
    stand: [],
    messages: 'messages',
    max: null
};

export default handleActions({
    [GET_STAND_START]: state => ({
        ...state,
        error: false,
        isLoadingstand: true,
    }),
        [GET_STAND_SUCCESS]: (state, action) => ({    
        ...state,
        stand: action.payload
    }),
    [GET_STAND_ERROR]: (state, action) => ({
        ...state,
        error: action.payload,
    }),
    [GET_STAND_FINALLY]: state => ({
        ...state,
        isLoadingstand: false
    }),


    [INSERT_STAND_START]: state => ({
        ...state,
        error: false,
        isLoadingstand: true,
    }),
    [INSERT_STAND_SUCCESS]: (state, action) => ({
        ...state,
        stand: [...state.stand, action.payload],
    }),
    [INSERT_STAND_ERROR]: (state, action) => ({
        ...state,
        error: action.payload,
    }),
    [INSERT_STAND_FINALLY]: state => ({
        ...state,
        isLoadingstand: false
    }),

}, initialState);
