
import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";
import {
    GET_ASISTENCIA,
    GET_ASISTENCIA_START,
    GET_ASISTENCIA_SUCCESS,
    GET_ASISTENCIA_ERROR,
    GET_ASISTENCIA_FINALLY,

    INSERT_ASISTENCIA,
    INSERT_ASISTENCIA_START,
    INSERT_ASISTENCIA_SUCCESS,
    INSERT_ASISTENCIA_ERROR,
    INSERT_ASISTENCIA_FINALLY
} from "../../redux/actions";

function* GetAsistenciaAction({ payload: {  } }) {
  yield put(GET_ASISTENCIA_START());


  try {


    const headers = {
      headers: {
        // Authorization: `Bearer ${token}`,
        'accept': 'application/json'
      },
    };

  
    const { data } = yield call(
      axios.get,
      import.meta.env.VITE_API_ASISTENCIA,
      headers
    );
    console.log('desde saga get', data)
    return yield put(GET_ASISTENCIA_SUCCESS(data));
  } catch (error) {
    console.log(error.message);
    yield put(GET_ASISTENCIA_ERROR(error.message));
  } finally {
    yield put(GET_ASISTENCIA_FINALLY());
  }
}

function* InsertAsistenciaAction({ payload: { stand } }) {
  yield put(INSERT_ASISTENCIA_START());
  // let token = keycloak.token;

  try {
    // const isTokenUpdated = yield call(keycloak.updateToken, 5);
    // token = isTokenUpdated ? keycloak.token : token;

    // const headers = {
    //   headers: {
    //     Authorization: `Bearer ${token}`,
    //     // 'accept': 'application/json'
    //   },
    // };


   
    const { data } = yield call(
      axios.post,
      import.meta.env.VITE_API_ASISTENCIA,
      stand
    );
//  console.log("desde saga campus", data)
    return yield put(INSERT_ASISTENCIA_SUCCESS(data));
  } catch (error) {

    console.log(INSERT_ASISTENCIA_ERROR(error.message));
    yield put(INSERT_ASISTENCIA_ERROR(error.message));
  } finally {
    yield put(INSERT_ASISTENCIA_FINALLY());
  }
}

export default function* actionsWatcher() {
  yield takeLatest(GET_ASISTENCIA, GetAsistenciaAction);
  yield takeLatest(INSERT_ASISTENCIA, InsertAsistenciaAction);
}
