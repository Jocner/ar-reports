import { createAction } from 'redux-actions';

export const GET_ASISTENCIA = createAction('GET_ASISTENCIA');
export const GET_ASISTENCIA_START = createAction('GET_ASISTENCIA_START');
export const GET_ASISTENCIA_SUCCESS = createAction('GET_ASISTENCIA_SUCCESS');
export const GET_ASISTENCIA_ERROR = createAction('GET_ASISTENCIA_ERROR');
export const GET_ASISTENCIA_FINALLY = createAction('GET_ASISTENCIA_FINALLY');

export const INSERT_ASISTENCIA = createAction('INSERT_ASISTENCIA');
export const INSERT_ASISTENCIA_START = createAction('INSERT_ASISTENCIA_START');
export const INSERT_ASISTENCIA_SUCCESS = createAction('INSERT_ASISTENCIA_SUCCESS');
export const INSERT_ASISTENCIA_ERROR = createAction('INSERT_ASISTENCIA_ERROR');
export const INSERT_ASISTENCIA_FINALLY = createAction('INSERT_ASISTENCIA_FINALLY');

