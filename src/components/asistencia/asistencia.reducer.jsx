import { handleActions } from 'redux-actions';

import {
   
    GET_ASISTENCIA_START,
    GET_ASISTENCIA_SUCCESS,
    GET_ASISTENCIA_ERROR,
    GET_ASISTENCIA_FINALLY,

    INSERT_ASISTENCIA_START,
    INSERT_ASISTENCIA_SUCCESS,
    INSERT_ASISTENCIA_ERROR,
    INSERT_ASISTENCIA_FINALLY
 

} from '../../redux/actions';

const initialState = {
    error: null,
    isLoadingasistencia: true,
    asistencia: [],
    messages: 'messages',
    max: null
};

export default handleActions({
    [GET_ASISTENCIA_START]: state => ({
        ...state,
        error: false,
        isLoadingasistencia: true,
    }),
    [GET_ASISTENCIA_SUCCESS]: (state, action) => ({    
        ...state,
        asistencia: action.payload
    }),
    [GET_ASISTENCIA_ERROR]: (state, action) => ({
        ...state,
        error: action.payload,
    }),
    [GET_ASISTENCIA_FINALLY]: state => ({
        ...state,
        isLoadingasistencia: false
    }),


    [INSERT_ASISTENCIA_START]: state => ({
        ...state,
        error: false,
        isLoadingasistencia: true,
    }),
    [INSERT_ASISTENCIA_SUCCESS]: (state, action) => ({
        ...state,
        asistencia: [...state.asistencia, action.payload],
    }),
    [INSERT_ASISTENCIA_ERROR]: (state, action) => ({
        ...state,
        error: action.payload,
    }),
    [INSERT_ASISTENCIA_FINALLY]: state => ({
        ...state,
        isLoadingasistencia: false
    }),

}, initialState);